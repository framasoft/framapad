[![](https://framasoft.org/nav/img/icons/ati/sites/git.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Framapad est un éditeur de texte collaboratif en ligne que Framasoft propose sur le site : https://framapad.org

Il repose sur le logiciel [Etherpad](http://etherpad.org/) que nous n'avons pas modifié.

Nous proposons simplement plusieurs instances accessibles depuis la page d'accueil dont le code se trouve dans ce dépôt.
Ce dépôt ne contient pas Etherpad, juste notre page d'accueil.

Si vous souhaitez traduire la page d’accueil, allez sur <https://weblate.framasoft.org/projects/framapad/>.

* * *
